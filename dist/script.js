// List videos in a video directory
// Show video thumbnails
// Download video on click
// Python server
function getVideoList() {
  var videoList = [];
  $.ajax({
    url: '/video-list',
    type: 'GET',
    async: false,
    success: function (data) {
      videoList = data;
    },
  });
  return videoList;
}
