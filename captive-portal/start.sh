#!/usr/bin/env bash

# portal
echo 'Downloading Portal'
rm /usr/src/dist.zip
wget -q https://gitlab.com/izzetemre/turnstick-portal/-/raw/main/dist.zip
rm -rf /usr/src/dist
zip -d your-archive.zip "__MACOSX*"
unzip dist.zip
mv /usr/src/dist/* /usr/src/

# Python
echo 'Starting Python server'
python /usr/src/captive-portal.py &
python /usr/src/redirect.py